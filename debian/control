Source: sane-airscan
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Section: graphics
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 gnutls-dev,
 libavahi-client-dev,
 libavahi-common-dev,
 libjpeg62-turbo-dev | libjpeg-turbo8-dev,
 libpng-dev,
 libsane-dev,
 libtiff-dev,
 libxml2-dev,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/printing-team/sane-airscan
Vcs-Git: https://salsa.debian.org/printing-team/sane-airscan.git
Homepage: https://github.com/alexpevzner/sane-airscan
Rules-Requires-Root: no

Package: sane-airscan
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ipp-usb,
 sane-utils,
Description: SANE backend for AirScan (eSCL) and WSD document scanner
 Similar to how most modern network printers support "driverless"
 printing, using the universal vendor-neutral printing protocol, many
 modern network scanners and MFPs support "driverless" scanning.
 .
 Driverless scanning comes in two flavors:
 .
 * Apple AirScan or AirPrint scanning (official protocol name is eSCL)
 * Microsoft WSD, or WS-Scan (term WSD means "Web Services for Devices)
 .
 This backend implements both protocols, choosing automatically between
 them. It was successfully tested with many devices from Brother, Canon,
 Kyocera, Lexmark, Epson, HP, Ricoh, Samsung and Xerox both in WSD and
 eSCL modes.
